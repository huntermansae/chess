package Chess;

public class Rook extends Queen {
	
	public Rook(boolean iswhite){
		super(iswhite);
	}

	@Override
	public boolean allowedMove(ChessPiece[][] board, int startx, int starty,
			int endx, int endy) {
		return rookMoves(board, startx, starty, endx, endy);
	}

}

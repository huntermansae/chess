package Chess;

public class Board {
	
	ChessPiece board[][] = new ChessPiece[8][8];
	
	public Board(){
		this.init();
	}
	
	public void move(int startx, int starty, int endx, int endy){
		if(startx < 8 && startx >= 0 && starty < 8 && starty >= 0 && endx < 8 && endx >= 0 && endy < 8 
				&& endy >= 0 && board[startx][starty] != null){
			set(board[startx][starty], endx, endy);
			set(null,startx, starty);
		}
	}
	
	public void set(ChessPiece piece, int x, int y){
		if(x < 8 && x >= 0 && y < 8 && y >= 0){
			board[x][y] = piece;
		}
	}
	
	/**
	 * check if the piece is yours!
	 * @param iswhite is the current turn , where true mean's its white's turn
	 * @param checkx the column 
	 * @param checky the row 
	 * @return whether the current player can select the piece
	 */
	public boolean isValid(boolean iswhite, int checkx, int checky){
		if(checkx < 8 && checkx >= 0 && checky < 8 && checky >= 0 && board[checkx][checky] != null){
			return board[checkx][checky].white == iswhite;
		}else{
			return false;
		}
	}
	
	public void init(){
		for(int x = 0; x < 8; x++){
			for(int y = 0; y < 8; y++){
				board[x][y] = null;
			}
		}
		
		board[0][0] = (ChessPiece) new Rook(true);
		board[1][0] = (ChessPiece) new Knight(true);
		board[2][0] = (ChessPiece) new Bishop(true);
		board[3][0] = (ChessPiece) new Queen(true);
		board[4][0] = (ChessPiece) new King(true);
		board[5][0] = (ChessPiece) new Bishop(true);
		board[6][0] = (ChessPiece) new Knight(true);
		board[7][0] = (ChessPiece) new Rook(true);
		for(int i = 0; i < 8; i++){
			board[i][1] = (ChessPiece) new Pawn(true);
			board[i][6] = (ChessPiece) new Pawn(false);
		}
		board[0][7] = (ChessPiece) new Rook(false);
		board[1][7] = (ChessPiece) new Knight(false);
		board[2][7] = (ChessPiece) new Bishop(false);
		board[3][7] = (ChessPiece) new Queen(false);
		board[4][7] = (ChessPiece) new King(false);
		board[5][7] = (ChessPiece) new Bishop(false);
		board[6][7] = (ChessPiece) new Knight(false);
		board[7][7] = (ChessPiece) new Rook(false);
		
	}
}

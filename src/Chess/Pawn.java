package Chess;

public class Pawn extends ChessPiece {

	boolean firstMove = true;
	
	public Pawn(boolean iswhite){
		super(iswhite);
		
		
	}

	@Override
	public boolean allowedMove(ChessPiece[][] board, int startx, int starty,
			int endx, int endy) {
		if (startx == endx) {
			if (white) {
				if (firstMove && endy - starty == 2 && board[endx][endy] == null && board[endx][endy - 1] == null) {
					firstMove = false;
					return true;
				}
				if (endy - starty == 1 && board[endx][endy] == null) {
					firstMove = false;
					return true;
				}
			} else {
				if (firstMove && endy - starty == -2 && board[endx][endy] == null && board[endx][endy + 1] == null) {
					firstMove = false;
					return true;
				}
				if (endy - starty == -1 && board[endx][endy] == null) {
					firstMove = false;
					return true;
				}
			}
		} else if (Math.abs(startx - endx) == 1) { // difference of x location is 1
			if (white && endy - starty == 1 && board[endx][endy] != null) {
				firstMove = false;
				return true;
			} else if (!white && endy - starty == -1 && board[endx][endy] != null) {
				firstMove = false;
				return true;
			}
		}
		
		// TODO CHESS en passante
		return false;
	}
}

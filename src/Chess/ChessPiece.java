package Chess;

public abstract class ChessPiece {
	boolean white;
	
	public ChessPiece(boolean iswhite){
		this.white = iswhite;
	}

	/**
	 * checks if this piece can move from start to destination based on current setup
	 * @param board every pieces' positions on the board, this is a board of array containing chess pieces
	 * @param startx current piece's start x column
	 * @param starty current piece's start y row 
	 * @param endx piece's final column x destination
	 * @param endy piece's final row y destination
	 * @return whether the current piece can move to the destination 
	 */
	public abstract boolean allowedMove(ChessPiece board[][], int startx, int starty, int endx, int endy);
}

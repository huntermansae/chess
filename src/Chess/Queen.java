package Chess;

public class Queen extends ChessPiece {

	public Queen(boolean iswhite){
		super(iswhite);
	}

	@Override
	public boolean allowedMove(ChessPiece[][] board, int startx, int starty,
			int endx, int endy) {
		return rookMoves(board, startx, starty, endx, endy) || bishopMoves(board, startx, starty, endx, endy);
	}
	
	public boolean rookMoves(ChessPiece[][] board, int startx, int starty,
			int endx, int endy) {
		// check if the "end" is up down or left right from the start
		
		boolean updownleftright = startx == endx || starty == endy;
		
		if (updownleftright == false) {
			return false;
		}
		
		// check if any spot in between end and start is not null
		
		boolean jump = false;
		
		if (startx == endx) { // vertical
			for (int y = starty + 1; y < endy; y++) {
				if (board[startx][y] != null){ // A piece exists within range of your movement
					jump = true; // you have encountered an obstacle
				}
			}
			for(int y = starty - 1; y > endy; y--) {
				if (board[startx][y] != null){ // A piece exists within range of your movement
					jump = true; // you have encountered an obstacle
				}
			}
		} else { //horizontal
			for (int x = startx + 1; x < endx; x++) {
				if (board[x][starty] != null){ // A piece exists within range of your movement
					jump = true; // you have encountered an obstacle
				}
			}
			for(int x = startx - 1; x > endx; x--) {
				if (board[x][starty] != null){ // A piece exists within range of your movement
					jump = true; // you have encountered an obstacle
				}
			}
		}
		
		return !jump;
	}

	public boolean bishopMoves(ChessPiece[][] board, int startx, int starty,
			int endx, int endy) {
		// check if the "end" is diagonal from the start
		
		boolean diagonal = Math.abs(endx - startx) == Math.abs(endy - starty);
		
		if (diagonal == false) {
			return false;
		}
		
		// check if any spot in between end and start is not null
		
		boolean jump = false;
		
		if (endx - startx == endy - starty) { // x and y are correlated
			for (int i = 1; i < endx - startx; i++) {
				if (board[startx + i][starty + i] != null){ // A piece exists within range of your movement
					jump = true; // you have encountered an obstacle
				}
			}
			for(int i = -1; i > endx - startx; i--) {
				if (board[startx + i][starty + i] != null){ // A piece exists within range of your movement
					jump = true; // you have encountered an obstacle
				}
			}
		} else { // x and y are inverse
			for (int i = 1; i < endx - startx; i++) {
				if (board[startx + i][starty - i] != null){ // A piece exists within range of your movement
					jump = true; // you have encountered an obstacle
				}
			}
			for(int i = -1; i > endx - startx; i--) {
				if (board[startx + i][starty - i] != null){ // A piece exists within range of your movement
					jump = true; // you have encountered an obstacle
				}
			}
		}
		
		return !jump;
	}
}

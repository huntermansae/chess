package Chess;
import renderer.ChessDisplayer;

public class ChessReferee {
	
	static Board chessBoard;
	static boolean white;
	static boolean playing;
	
	public static void start(){
		chessBoard = new Board();
		white = true;
		playing = true;
		ChessDisplayer.initialize();
		ChessDisplayer.place(ChessDisplayer.ChessPiece.BLACK_KING, 0, 0);
		delete(0, 0);
		for(int i = 0; i < 8 ; i++){
			for(int j = 0; j < 8; j++){
				ChessPiece piece = chessBoard.board[i][j];
				place(piece, i, j);
			}
		}
		play();
		
	}
	
	private static void play(){
		int startx = 0, starty = 0, endx = 0, endy = 0;
		
		while(playing){
			// get input
			int move[] =ChessDisplayer.nextMove();
			startx = move[0];
			starty = move[1];
			endx = move[2];
			endy = move[3];
			System.out.println("" + startx + " " + starty + " " + endx + " " + endy + " " );
			
			// verify that this piece is valid for the current player!
			// line saying if the current player can select piece and his intended destination is empty or enemy piece
			if(chessBoard.isValid(white, startx, starty) && !chessBoard.isValid(white, endx, endy)){
				
				// verify that piece at start can move to end legally
				if(chessBoard.board[startx][starty].allowedMove(chessBoard.board, startx, starty, endx,endy)){
					System.out.println("You can move your piece to the designated spot! Enemy's turn !");
					
					//Now I want to change the boolean white
					white = !white;
					
					//Actually move the piece
					chessBoard.set(null, endx, endy);
					delete(endx, endy);
					chessBoard.move(startx, starty, endx, endy);
					place(chessBoard.board[endx][endy], endx, endy);
					delete(startx, starty);
					
					//check if the king is taken!
					//TODO CHESS king is taken
				}
			}
			
		}
	}
	
	private static void place(ChessPiece piece, int i, int j) {
		if (piece == null) {
			ChessDisplayer.place(ChessDisplayer.ChessPiece.EMPTY, i, j);
		} else if (piece instanceof Pawn) {
			if (piece.white) {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.WHITE_PAWN, i, j);
			} else {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.BLACK_PAWN, i, j);
			}
		} else if (piece instanceof Knight) {
			if (piece.white) {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.WHITE_KNIGHT, i, j);
			} else {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.BLACK_KNIGHT, i, j);
			}
		} else if (piece instanceof Bishop) {
			if (piece.white) {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.WHITE_BISHOP, i, j);
			} else {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.BLACK_BISHOP, i, j);
			}
		} else if (piece instanceof Rook) {
			if (piece.white) {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.WHITE_ROOK, i, j);
			} else {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.BLACK_ROOK, i, j);
			}
		} else if (piece instanceof Queen) {
			if (piece.white) {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.WHITE_QUEEN, i, j);
			} else {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.BLACK_QUEEN, i, j);
			}
		} else if (piece instanceof King) {
			if (piece.white) {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.WHITE_KING, i, j);
			} else {
				ChessDisplayer.place(ChessDisplayer.ChessPiece.BLACK_KING, i, j);
			}
		}
	}
	
	private static void delete(int i, int j) {
		ChessDisplayer.remove(i, j);
	}
}

package Chess;

public class King extends ChessPiece{
	
	boolean moved;

	public King(boolean iswhite){
		super(iswhite);
		moved = false;
	}
	
	@Override
	public boolean allowedMove(ChessPiece board[][], int startx, int starty, int endx, int endy) {
	 if(Math.abs(endx - startx) <= 1 && Math.abs(endy - starty) <= 1 ){
		 // this mean the king can make a move that is not a castle move
		 /*
		 for(int i = 0; i < 8; i++ ){
			 for(int j = 0; j < 8; j++){
				 ChessPiece piece = board[i][j];
				 if (piece == null) {
					 continue;
				 }
				 if(this.white != piece.white){
					 if (piece.allowedMove(board, i, j, endx, endy) == true) {
						 return false;
					 }
				 }
			 }
		 }
		 */
		 moved = true;
		 return true;
	 } else if (!moved && Math.abs(endx -startx) == 2 && (endy == starty)) {
		 // the king is castling
		 if (endx < startx) {
			 for(int x = startx - 1; x >= endx; x--) {
				 for(int i = 0; i < 8; i++ ){
					 for(int j = 0; j < 8; j++){
						 ChessPiece piece = board[i][j];
						 if (piece == null) {
							 continue;
						 }
						 if(this.white != piece.white){
							 if (piece.allowedMove(board, i, j, endx, endy) == true) {
								 return false;
							 }
						 }
					 }
				 }
			 }
			 for(int x = startx - 1; x > 0; x--) {
				 ChessPiece piece = board[x][starty];
				 if (piece != null) {
					 return false;
				 }
			 }
			 ChessPiece piece = board[0][starty];
			 if (piece instanceof Rook) {
				 moved = true;
				 return true;
			 } else {
				 return false;
			 }
		 } else {
			 for(int x = startx + 1; x <= endx; x++) {
				 for(int i = 0; i < 8; i++ ){
					 for(int j = 0; j < 8; j++){
						 ChessPiece piece = board[i][j];
						 if (piece == null) {
							 continue;
						 }
						 if(this.white != piece.white){
							 if (piece.allowedMove(board, i, j, endx, endy) == true) {
								 return false;
							 }
						 }
					 }
				 }
			 }
			 for(int x = startx + 1; x < 7; x++) {
				 ChessPiece piece = board[x][starty];
				 if (piece != null) {
					 return false;
				 }
			 }
			 ChessPiece piece = board[7][starty];
			 if (piece instanceof Rook) {
				 moved = true;
				 return true;
			 } else {
				 return false;
			 }
		 }
	 } else {
		 //illegal move!
		 return false;
	 }
	}
}

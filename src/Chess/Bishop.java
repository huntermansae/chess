package Chess;

public class Bishop extends Queen  {

	public Bishop(boolean iswhite){
		super(iswhite);
	}

	@Override
	public boolean allowedMove(ChessPiece[][] board, int startx, int starty,
			int endx, int endy) {
		return bishopMoves(board, startx, starty, endx, endy);
	}
}

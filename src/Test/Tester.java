package Test;

import java.lang.reflect.InvocationTargetException;

public class Tester {

	static boolean QUIET = true;
	
	public static void main(String[] args) {
		System.out.println("Starting Tests for Chess");
		
		test(Tester.class);
		test(TestRook.class);
	}
	
	public static String runTests() {
		return "Testing begun";
	}
	
	public static void quiet(String s) {
		if (!QUIET) {
			System.out.print(s);
		}
	}
	
	public static void quietln(String s) {
		if (!QUIET) {
			System.out.println(s);
		}
	}
	
	/**
	 * Invokes the runTests static method of the test class passed in
	 * @param testClass - class that extends Tester that we want to run tests on
	 */
	private static void test(Class<? extends Tester> testClass) {
		try {
			System.out.println(testClass.getMethod("runTests", (Class<?>[]) null).invoke(null, (Object[]) null));
			System.out.println();
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// ignore
		}
	}
}

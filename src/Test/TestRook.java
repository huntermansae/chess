package Test;

import Chess.ChessPiece;
import Chess.Pawn;
import Chess.Rook;

public class TestRook extends Tester{

	public static String runTests() {
		int passed = 0;
		
		// Want to test if Rook can go straight up if there's no obstacle!
		System.out.println("Testing Rook moves up properly");
		passed += testMoveUp();
		
		// Want to test if Rook can go straight down!
		System.out.println("Testing Rook moves down properly");
		passed += testMoveDown();

		//Want to test if Rook can go left and right!
		System.out.println("Testing Rook moves horizontally properly");
		passed += testMoveHorizontal();
		
		// Want to test that Rook can't step over a piece
		System.out.println("Testing Rook does not jump over pieces");
		passed += testNoJump();
		
		return "Finished running TestRook Suites. Passed (" + passed + " / 11)";
	}
	
	private static int testMoveUp() {
		int passed = 0;
		
		//put Rook in orig spot(0,0) 
		ChessPiece board[][] = new ChessPiece[8][8];
		ChessPiece rook = new Rook(true);
		board[0][0] = rook;
		
		// try placing rook into (0,2) with no obstacle
		quiet("Trying to move Rook from 0,0 to 0,2: ");

		// expect true 
		if(rook.allowedMove(board, 0,0,0,2)){
			quietln("test passed!");
			passed++;
		} else {
			quietln("test failed!");
		}
		
		// try placing rook into (0,7) with no obstacle
		quiet("Trying to move Rook from 0,0 to 0,7: ");

		// expect true
		if(rook.allowedMove(board, 0,0,0,7)){
			quietln("test passed!");
			passed++;
		} else {
			quietln("test failed!");
		}
	
		// try placing rook into (1,1) with no obstacle
		quiet("Rook should not be able to move from 0,0 to 1,1: ");

		// expect false
		if(rook.allowedMove(board, 0,0,1,1)){
			quietln("test failed!Your Rook moved to an invalid location!");
		} else {
			quietln("test passed!Your Rook cannot indeed move to the designated location!");
			passed++;
		}
		
		quietln("Passed " + passed + " out of 3 tests.\n");
		
		return passed;
	}
	
	private static int testMoveDown() {
		int passed = 0;
		
		//put Rook in far up spot(0,7) 
		ChessPiece board[][] = new ChessPiece[8][8];
		ChessPiece rook = new Rook(true);
		board[0][7] = rook;
		
		// try placing rook into (0,2) with no obstacle
		quiet("Trying to move Rook from 0,7 to 0,2: ");

		// expect true 
		if(rook.allowedMove(board, 0,7,0,2)){
			quietln("test passed!");
			passed++;
		} else {
			quietln("test failed!");
		}
		
		// try placing rook into (0,0) with no obstacle
		quiet("Trying to move Rook from 0,7 to 0,0: ");

		// expect true
		if(rook.allowedMove(board, 0,7,0,0)){
			quietln("test passed!");
			passed++;
		} else {
			quietln("test failed!");
		}
	
		// try placing rook into (1,6) with no obstacle
		quiet("Rook should not be able to move from 0,7 to 1,6: ");

		// expect false
		if(rook.allowedMove(board, 0,7,1,6)){
			quietln("test failed!Your Rook moved to an invalid location!");
		} else {
			quietln("test passed!Your Rook cannot indeed move to the designated location!");
			passed++;
		}
		
		quietln("Passed " + passed + " out of 3 tests.\n");
		
		return passed;
	}

	private static int testMoveHorizontal() {
		int passed = 0;
		
		//put Rook in far up spot(1,0) 
		ChessPiece board[][] = new ChessPiece[8][8];
		ChessPiece rook = new Rook(true);
		board[1][0] = rook;
		
		// try placing rook into (0,0) with no obstacle
		quiet("Trying to move Rook from 1,0 to 0,0: ");

		// expect true 
		if(rook.allowedMove(board, 1,0,0,0)){
			quietln("test passed!");
			passed++;
		} else {
			quietln("test failed!");
		}
		
		// try placing rook into (2,0) with no obstacle
		quiet("Trying to move Rook from 1,0 to 2,0: ");

		// expect true
		if(rook.allowedMove(board, 1,0,2,0)){
			quietln("test passed!");
			passed++;
		} else {
			quietln("test failed!");
		}
	
		// try placing rook into (2,1) with no obstacle
		quiet("Rook should not be able to move from 1,0 to 2,1: ");

		// expect false
		if(rook.allowedMove(board, 1,0,2,1)){
			quietln("test failed!Your Rook moved to an invalid location!");
		} else {
			quietln("test passed!Your Rook cannot indeed move to the designated location!");
			passed++;
		}
		
		
		// try placing rook into (0,1) with no obstacle
		quiet("Rook should not be able to move from 1,0 to 0,1: ");

		// expect false
		if(rook.allowedMove(board, 1,0,0,1)){
			quietln("test failed!Your Rook moved to an invalid location!");
		} else {
			quietln("test passed!Your Rook cannot indeed move to the designated location!");
			passed++;
		}
		
		quietln("Passed " + passed + " out of 4 tests.\n");
		
		return passed;
	}

	private static int testNoJump() {
		int passed = 0;
		
		//put Rook in orig spot(0,0) 
		ChessPiece board[][] = new ChessPiece[8][8];
		ChessPiece rook = new Rook(true);
		board[3][3] = rook;
		board[3][5] = new Pawn(false);
		
		// try placing rook into (3,7) with obstacle
		quiet("Rook can't jump from 3,3 to 3,7: ");

		// expect true 
		if(rook.allowedMove(board, 3,3,3,7)){
			quietln("test failed!");
		} else {
			quietln("test passed!");
			passed++;
		}
		
		quietln("Passed " + passed + " out of 1 test.\n");
		
		return passed;
	}
}
